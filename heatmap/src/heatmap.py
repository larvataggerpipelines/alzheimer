import seaborn as sns
import pandas as pd
import os
from matplotlib import pyplot as plt

datapath = '../data/processed'

for model in os.scandir(datapath):
    if not model.is_dir():
        continue
    for confusion in os.scandir(model.path):
        if not confusion.name.endswith('.csv'):
            continue

        cm = pd.read_csv(confusion.path)
        cm.index = cm.columns

        ax = sns.heatmap(cm, annot=True, fmt='.0f', linewidth=.5)

        figpath = os.path.join(model.path, os.path.splitext(confusion.name)[0] + ".png")
        ax.get_figure().savefig(figpath)
        plt.close()

        scaled_cm = cm / cm.sum(axis=0)

        ax = sns.heatmap(scaled_cm, annot=True, fmt='.2f', linewidth=.5)

        figpath = os.path.join(model.path, os.path.splitext(confusion.name)[0] + "_scaled.png")
        ax.get_figure().savefig(figpath)
        plt.close()
