# Alzheimer tagger training

This code repository focuses on exploring different training procedures of a tagger suitable for a project related to Alzheimer's disease.

The main computations are to be done on the Maestro cluster, in the user's dedicated scratch space. Once logged in on Maestro:
```
cd $MYSCRATCH
git clone https://gitlab.com/larvataggerpipelines/Alzheimer
cd Alzheimer
```

In the present version, the data are prepared with:
```
make data/interim
```

This step creates the *data/raw* and *data/interim* directories. At present, data files are picked from 3 different locations. Data preparation depends on the location. For example, some label files are patched to convert the original rolls (tag *roll_large* alone, without the *edited* secondary tag) into small actions (tag *small_motion*).
This makes sense with the *trainingData_staticBend* dataset, because static bends were labeled as rolls under the assumption there were no rolls, but some time steps originally labeled as rolls were found afterwards.

Other data preparation steps include cropping to a predefined time segment or merging multiple label files with manual editions in distinct time segments.

Once `make data/interim` is complete, the main step consists of:
```
make
```
This trains a new tagger and then generates labels for the same data files using the new tagger. The tagging step is launched as Slurm jobs with `sbatch`. These jobs run in the background and must be waited for before moving on to any other processing step. Use `squeue --me` to check for progress.

The trained tagger can be packaged into a zip file with `make model`. This can be done before the retagging jobs are complete, *i.e.* as soon as the command-line is interactive again.


**Broken!** Once the retagging jobs are complete (output from `squeue --me` is empty), a third step generates confusion matrices with:
```
make confusion
```

Figure generation is done, preferably on a personal computer:
```
git clone https://gitlab.com/larvataggerpipelines/Alzheimer
cd Alzheimer/heatmap
make install
make import
make figures
```
Note that the Maestro cluster is expected to be set up for ssh on the personal computer as host *maestro*.

For reproducibility, use of python version 3.10 is enforced (command `python3.10` is expected).

The adjustable training parameters can be found in the top-level `Makefile` file. The variables are named `SEGMENT_BEGIN`, `SEGMENT_END`, `ITER_TRAIN`, `ITER_FINETUNE`. Once these parameters are modified, the full procedure from `make retrain` can be similarly applied (data preparation excluded). Most of the generated files are stored in separate directories.

