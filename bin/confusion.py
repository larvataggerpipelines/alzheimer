import os
from glob import glob
import numpy as np
from sklearn.metrics import confusion_matrix
from taggingbackends.data.labels import Labels
from taggingbackends.data.dataset import LarvaDataset
from taggingbackends.explorer import BackendExplorer
from maggotuba.data.make_dataset import make_dataset
from maggotuba.models.predict_model import predict_model

trained_model = os.getenv('TRAINED_MODEL')

def index(labels, tags):
    if isinstance(tags, str):
        if tags == 'edited':
            # probably a (manual) tagging mistake
            return -1
        else:
            return labels.index(tags)
    else:
        for i, label in enumerate(labels):
            if label in tags:
                return i
    print(tags)
    print("incompatible labels")
    return -1

for task, fn_true, fn_pred in (
        ('test', 'cropped.label', 'predicted-cropped.label'),
    ):

    labels = None
    cm = None

    for expected in glob(f'/data/interim/**/{fn_true}', recursive=True):
        assay = os.path.relpath(os.path.dirname(expected), '/data/interim')
        print(assay)
        expected = Labels(expected)
        try:
            predicted = Labels(os.path.join('/data/processed', trained_model, assay, fn_pred))
        except FileNotFoundError:
            print(f'no predicted labels found in assay {assay}')
            continue
        if labels is None:
            labels = predicted.labelspec
        else:
            assert labels == predicted.labelspec
        for larva in expected:
            # note: not all the larvae in `expected` can be found in `predicted`
            y_pred = np.array([labels.index(label) for label in predicted[larva].values()])
            y_true= np.array([index(labels, tags) for tags in expected[larva].values()])
            assert len(y_pred) == len(y_true)
            ok = 0 <= y_true
            cm_ = confusion_matrix(y_true[ok], y_pred[ok], labels=range(len(labels)))
            cm = cm_ if cm is None else cm + cm_

    assert cm is not None
    print(labels)
    print(cm)

    with open(f'/data/processed/{trained_model}/confusion-{task}.csv', 'w') as f:
        f.write(",".join(labels))
        for row in cm:
            f.write("\n")
            f.write(",".join([str(count) for count in row]))

##

task = 'train'

dir = os.path.join("/data/apptainer", trained_model)

training_dataset = glob(os.path.join(dir, "larva_dataset_*.hdf5"))

if not training_dataset:
    print('`make` should have copied the larva_dataset file')
    os.makedirs(dir, exist_ok=True)
    altdir = os.path.join("/data/apptainer/interim", trained_model)
    training_dataset = glob(os.path.join(altdir, "larva_dataset_*.hdf5"))
    for srcfile in training_dataset:
        dstfile = os.path.join(dir, os.path.basename(srcfile))
        with open(srcfile, 'rb') as src:
            with open(dstfile, 'wb') as dst:
                dst.write(src.read())
    training_dataset = dstfile

elif 1 < len(training_dataset):
    print('several larva_dataset files found')

training_dataset = training_dataset[-1]

labels = LarvaDataset(training_dataset).labels
if isinstance(labels[0], bytes):
    labels = [label.decode('utf8') for label in labels]

backend = BackendExplorer(model_instance=trained_model)
backend.reset_data()
backend.move_to_raw(training_dataset, copy=False)
make_dataset(backend, labels_expected=True)
predicted, expected = predict_model(backend)
if isinstance(expected[0], bytes):
    expected = [s.decode('utf8') for s in expected]

cm = confusion_matrix(expected, predicted)

print(labels)
print(cm)

with open(f'/data/processed/{trained_model}/confusion-{task}.csv', 'w') as f:
    f.write(",".join(labels))
    for row in cm:
        f.write("\n")
        f.write(",".join([str(count) for count in row]))

