# on every change in data repositories, run `make clean` before running `make data/interim` again
RAWLABELDATA:=/pasteur/zeus/projets/p02/hecatonchire/anqi/RECLASSFICATION
RAWTRACKDATA:=/pasteur/zeus/projets/p02/hecatonchire/tihana/t2

DATA_LABEL:=more-static-bend

LARVATAGGER_VERSION:=0.17
APPTAINER_VERSION:=1.3.1

LARVATAGGER_IMAGE:=images/larvatagger-$(LARVATAGGER_VERSION).sif

SLURM_OPTIONS:=-p dbc -q dbc
TRAIN_CPUS:=32
PREDICT_CPUS:=4

SEGMENT_BEGIN:=30
SEGMENT_END:=80
ITER_TRAIN:=500
ITER_FINETUNE:=9500
TRAIN_OPTIONS:=--balancing-strategy=maggotuba --iterations $(ITER_TRAIN),$(ITER_FINETUNE) --labels back_large,cast_large,hunch_large,run_large,roll_large,stop_large,small_motion
PRETRAINED_MODEL:=t2-5class-1-25
ifdef PRETRAINED_MODEL
	DATA_LABEL:=$(DATA_LABEL)-with-custom-encoder
	TRAIN_OPTIONS:=--pretrained-model $(PRETRAINED_MODEL) $(TRAIN_OPTIONS)
	BIND_EXTRA:=--bind pretrained_models:/app/MaggotUBA/pretrained_models
endif
TRAINED_MODEL:=$(SEGMENT_BEGIN)-$(SEGMENT_END)-$(ITER_TRAIN)-$(ITER_FINETUNE)-$(DATA_LABEL)

.PHONY: larvatagger train predict confusion retrain clean model anqi1 anqi2 tihana
.ONESHELL: $(LARVATAGGER_IMAGE) data/interim models/$(TRAINED_MODEL) predict bin/predict anqi1 anqi2 tihana

# default target; includes both train and predict steps; requires data preparation has been done beforehands
retrain:
	rm -rf models/$(TRAINED_MODEL)
	rm -rf data/apptainer/$(TRAINED_MODEL)
	rm -rf data/processed/$(TRAINED_MODEL)
	rm -rf data/interim/*/predicted*.label
	rm -rf log
	$(MAKE) train
	$(MAKE) predict
# `make predict` spawns Slurm jobs. Once the jobs are complete, run `make confusion`

train: models/$(TRAINED_MODEL)
	cp -Rp data/apptainer/interim/$(TRAINED_MODEL) data/apptainer/$(TRAINED_MODEL)

models/$(TRAINED_MODEL): data/interim $(LARVATAGGER_IMAGE)
	mkdir -p models
	mkdir -p data/apptainer
	[ "`module --version 2>&1`" = "-bash: module: command not found" ] \
	  || module load apptainer/$(APPTAINER_VERSION)
	srun $(SLURM_OPTIONS) apptainer exec \
		--bind data/interim:/data \
		--bind models:/app/MaggotUBA/models \
		--bind data/apptainer:/app/MaggotUBA/data \
		$(BIND_EXTRA) \
		--env JULIA_THREADS=$(TRAIN_CPUS) \
		$(LARVATAGGER_IMAGE) \
		/app/scripts/larvatagger-toolkit.jl train /app/MaggotUBA /data/**/cropped.label $(TRAINED_MODEL) \
		$(TRAIN_OPTIONS)

predict: bin/predict
	mkdir -p data/apptainer
	mkdir -p log
	[ "`module --version 2>&1`" = "-bash: module: command not found" ] \
	  || module load apptainer/$(APPTAINER_VERSION)
	sbatch $(SLURM_OPTIONS) --cpus-per-task=$(PREDICT_CPUS) --output=log/predict-%A_%a.log \
		--job-name=Anqi-predict --array=1-`find data/interim -name trx.mat | wc -l` bin/predict
	squeue --me

bin/predict: .overwrite
	mkdir -p bin
	cat <<- "EOF" > $@
	#!/usr/bin/env bash
	#SBATCH --cpus-per-task=$(PREDICT_CPUS)
	file=`find data/interim -name trx.mat | head -n$$SLURM_ARRAY_TASK_ID | tail -1`
	dir=`dirname $$file`
	srun apptainer exec \
		--bind $$dir:/data \
		--bind models:/app/MaggotUBA/models \
		--bind data/apptainer:/app/MaggotUBA/data \
		--env JULIA_THREADS=$(PREDICT_CPUS) \
		$(LARVATAGGER_IMAGE) \
		/app/scripts/larvatagger-toolkit.jl predict /app/MaggotUBA $(TRAINED_MODEL) \
			/data/trx.mat --data-isolation
	# TODO: crop following the same windowing rules as in target `data/interim`
	srun apptainer exec \
		--bind $$dir:/data \
		--env JULIA_THREADS=$(PREDICT_CPUS) \
		$(LARVATAGGER_IMAGE) \
		/app/scripts/larvatagger-toolkit.jl import /data/predicted.label predicted-cropped.label \
			--segment=$(SEGMENT_BEGIN),$(SEGMENT_END)
	EOF
	chmod a+x $@

# the below target is broken by files cropped to time windows other than 30-80s
confusion:
	for file in `cd data/interim; find . -name predicted.label`; do \
		dir=`dirname $$file`; \
		mkdir -p data/processed/$(TRAINED_MODEL)/$$dir && \
		cp data/interim/$$dir/predicted*.label data/processed/$(TRAINED_MODEL)/$$dir/; \
	done
	[ "`module --version 2>&1`" = "-bash: module: command not found" ] \
	  || module load apptainer/$(APPTAINER_VERSION)
	apptainer run --bind data:/data --bind bin:/app/MaggotUBA/bin \
		--bind models:/app/MaggotUBA/models \
		--bind data/apptainer:/app/MaggotUBA/data \
		--env TRAINED_MODEL=$(TRAINED_MODEL) \
		$(LARVATAGGER_IMAGE) bin/confusion.py

# target for data preparation
data/interim: data/raw $(LARVATAGGER_IMAGE)
	[ "`module --version 2>&1`" = "-bash: module: command not found" ] \
	  || module load apptainer/$(APPTAINER_VERSION)
	for dir in `cd data/raw; find . -name trx.mat`; do \
		dir=`dirname $$dir`; \
		cd $(CURDIR) && \
		mkdir -p data/interim/$$dir && \
		cp -Rp data/raw/$$dir/* data/interim/$$dir/ && \
		cd data/interim/$$dir; \
		if [ "`ls *s.label | wc -l`" -eq 1 ]; then \
			file=`ls *s.label`; \
			if [ "$$file" == "30-60s.label" ]; then \
				apptainer exec --bind .:/data $(CURDIR)/$(LARVATAGGER_IMAGE) \
					/app/scripts/larvatagger-toolkit.jl import /data/30-60s.label cropped.label \
					--segment=30,60; \
			elif [ "$$file" == "60-80s.label" ]; then \
				apptainer exec --bind .:/data $(CURDIR)/$(LARVATAGGER_IMAGE) \
					/app/scripts/larvatagger-toolkit.jl import /data/60-80s.label cropped.label \
					--segment=60,80; \
			elif [ "$$file" == "30-80s.label" ]; then \
				apptainer exec --bind .:/data $(CURDIR)/$(LARVATAGGER_IMAGE) \
					/app/scripts/larvatagger-toolkit.jl import /data/30-80s.label cropped.label \
					--segment=30,80; \
			else \
				echo "cropping failed"; \
			fi; \
		elif [ "`ls *s.label | wc -l`" -gt 1 ]; then \
			apptainer exec --bind .:/data $(CURDIR)/$(LARVATAGGER_IMAGE) \
				/app/scripts/larvatagger-toolkit.jl merge /data/`ls *s.label | tail -n1` `ls *s.label | head -n1` merged.label; \
			apptainer exec --bind .:/data $(CURDIR)/$(LARVATAGGER_IMAGE) \
				/app/scripts/larvatagger-toolkit.jl import /data/merged.label cropped.label \
				--segment=$(SEGMENT_BEGIN),$(SEGMENT_END); \
		else \
			ln -s full.label cropped.label; \
		fi; \
	done

data/raw:
	$(MAKE) RAWLABELDATA=$(RAWLABELDATA)/trainingData_staticBend anqi1
	$(MAKE) RAWLABELDATA=$(RAWLABELDATA)/trainingData_moreStaticBend anqi2
	$(MAKE) tihana

images/larvatagger-%.sif:
	mkdir -p {images,models}
	cat <<- "EOF" > images/recipe-$*.def
	Bootstrap: docker
	From: flaur/larvatagger:$*-20230311
	%environment
	  export JULIA_DEPOT_PATH="/tmp:/usr/local/share/julia"
	%setup
	  cp requirements.txt $${SINGULARITY_ROOTFS}/app/MaggotUBA/
	%post
	  cd /app/MaggotUBA && poetry add $$(cat requirements.txt)
	%runscript
	  cd /app/MaggotUBA && poetry run python $$@
	EOF
	[ "$$(module --version 2>&1)" = "-bash: module: command not found" ] \
	  || module load apptainer/$(APPTAINER_VERSION)
	apptainer build $@ images/recipe-$*.def

larvatagger:
	$(MAKE) $(LARVATAGGER_IMAGE)

.overwrite:

clean:
	rm -rf data

# converts the original (standalone) roll_large tags into small_motion so that
# the manually edited roll_large are the only "rolls".
# Indeed, they actually are static bends labeled as roll_large under the false
# assumption there were no roll_large in the database.
anqi1:
	for dir in `cd $(RAWLABELDATA); find . -name trx.mat`; do \
		dir=`dirname $$dir`; \
		cd $(CURDIR) && \
		mkdir -p data/raw/$$dir && \
		cd data/raw/$$dir; \
		if ! [ -f trx.mat ]; then cp $(RAWLABELDATA)/$$dir/trx.mat .; fi; \
		for file in `find $(RAWLABELDATA)/$$dir -name *s.label`; do \
			patchedfile=`basename $$file`; \
			roll=`grep -n "roll_large" $$file | head -n1 | cut -d: -f1`; \
			if [ -n "$$roll" ]; then \
				small=`grep -n "small_motion" $$file | head -n1 | cut -d: -f1`; \
				origin=`grep -n '"labels": ' $$file | head -n1 | cut -d: -f1`; \
				if [ -n "`head -n$$origin $$file | tail -n1 | grep '{'`" ]; then \
					origin=$$(( origin + 1 )); \
				fi; \
				roll=$$(( roll - origin )); \
				small=$$(( small - origin )); \
				echo "$$file $$roll $$small"; \
				while IFS="" read -r p || [ -n "$$p" ]; do \
					if [ -n "`echo $$p | grep '"labels": '`" ]; then \
						printf '%s\n' "$$p" | sed "s/,$$roll,/,$$small,/g" | sed "s/,$$roll,/,$$small,/g"; \
					else \
						printf '%s\n' "$$p"; \
					fi; \
				done <$$file >$$patchedfile; \
			else \
				cp $$file $$patchedfile; \
			fi; \
		done; \
	done

# the next two targets process label files generated using a first tagger
# readily labeling static bends as roll_large.

# trainingData_moreStaticBend includes two files that cause non-blocking errors
anqi2:
	for dir in `cd $(RAWLABELDATA); find . -name trx.mat`; do \
		dir=`dirname $$dir`; \
		cd $(CURDIR) && \
		mkdir -p data/raw/$$dir && \
		cd data/raw/$$dir; \
		if ! [ -f trx.mat ]; then cp $(RAWLABELDATA)/$$dir/trx.mat .; fi; \
		cp $(RAWLABELDATA)/$$dir/*s.label .; \
	done

tihana:
	for file in 20210917_101927/20240129_175337.label 20210112_151020/20240129_142059.label 20201103_145318/20240129_163633.label; do
		rawdir=`cd $(RAWTRACKDATA); ls -d */p_*/$$(dirname $$file)`; \
		dir=`echo $$rawdir | cut -d/ -f1`/`dirname $$file`; \
		cd $(CURDIR) && \
		mkdir -p data/raw/$$dir && \
		cd data/raw/$$dir; \
		if ! [ -f trx.mat ]; then cp $(RAWTRACKDATA)/$$rawdir/trx.mat .; fi; \
		cp $(RAWLABELDATA)/$$file full.label; \
		sed -e 's/updated_trx_v[[:digit:]]\+/trx/' \
			-e "s/\"sha1\": \".*\"/\"sha1\": \"$$(sha1sum trx.mat | cut -d\  -f1)\"/" \
			-i full.label; \
	done

# package the model files in an archive so that they can be conveniently sent over the internet
model:
	rm -f models/$(TRAINED_MODEL).zip
	$(MAKE) models/$(TRAINED_MODEL).zip

models/%.zip:
	cd models && zip $*.zip $*/*

